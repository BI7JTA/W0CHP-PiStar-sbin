#!/bin/bash

# placeholder for any slipstream tasks

# This part fully-disables read-only mode in Pi-Star and
# W0CHP-PiStar-Dash installations.
#
# 1/2023 - W0CHP (updated on 2/23/2023)
#
if grep -qo ',ro' /etc/fstab ; then
    sed -i 's/defaults,ro/defaults,rw/g' /etc/fstab
    sed -i 's/defaults,noatime,ro/defaults,noatime,rw/g' /etc/fstab
fi
if grep -qo 'remount,ro' /etc/bash.bash_logout ; then
    sed -i '/remount,ro/d' /etc/bash.bash_logout
fi
if grep -qo 'fs_mode:+' /etc/bash.bashrc ; then
    sed -i 's/${fs_mode:+($fs_mode)}//g' /etc/bash.bashrc
fi
if grep -qo 'remount,ro' /usr/local/sbin/pistar-hourly.cron ; then
    sed -i '/# Mount the disk RO/d' /usr/local/sbin/pistar-hourly.cron
    sed -i '/mount -o remount,ro/d' /usr/local/sbin/pistar-hourly.cron
fi
if grep -qo 'remount,ro' /etc/rc.local ; then
    sed -i '/remount,ro/d' /etc/rc.local
fi
if grep -qo 'remount,ro' /etc/apt/apt.conf.d/100update ; then
    sed -i '/remount,ro/d' /etc/apt/apt.conf.d/100update
fi
if grep -qo 'remount,ro' /lib/systemd/system/apt-daily-upgrade.service ; then
    sed -i '/remount,ro/d' /lib/systemd/system/apt-daily-upgrade.service
    systemctl daemon-reload 
fi
if grep -qo 'remount,ro' /lib/systemd/system/apt-daily.service ; then
    sed -i '/remount,ro/d' /lib/systemd/system/apt-daily.service
    systemctl daemon-reload 
fi
if grep -qo 'remount,ro' /etc/systemd/system/apt-daily-upgrade.service ; then
    sed -i '/remount,ro/d' /etc/systemd/system/apt-daily-upgrade.service
    systemctl daemon-reload 
fi
if grep -qo 'remount,ro' /etc/systemd/system/apt-daily.service ; then
    sed -i '/remount,ro/d' /etc/systemd/system/apt-daily.service
    systemctl daemon-reload 
fi
#

# Git URI changed when transferring repos from me to the org.
#
# 2/2023 - W0CHP
#
function gitURIupdate () {
    dir="$1"
    gitRemoteURI=$(git --work-tree=${dir} --git-dir=${dir}/.git config --get remote.origin.url)

    git --work-tree=${dir} --git-dir=${dir}/.git config --get remote.origin.url | grep 'Chipster' &> /dev/null
    if [ $? == 0 ]; then
        newURI=$( echo $gitRemoteURI | sed 's/Chipster/WPSD-Dev/' )
        git --work-tree=${dir} --git-dir=${dir}/.git remote set-url origin $newURI
    fi
}
gitURIupdate "/var/www/dashboard"
gitURIupdate "/usr/local/bin"
gitURIupdate "/usr/local/sbin"
#

# Config backup file name change, so lets address that
#
# 5/2023 W0CHP
#
if grep -q 'Pi-Star_Config_\*\.zip' /etc/rc.local ; then
    sed -i 's/Pi-Star_Config_\*\.zip/WPSD_Config_\*\.zip/g' /etc/rc.local
fi
#

# migrated from other scripts to centralize
#
# 5/2023 W0CHP
#
# cleanup legacy naming convention
if grep -q 'modemcache' /etc/rc.local ; then
    sed -i 's/modemcache/hwcache/g' /etc/rc.local
    sed -i 's/# cache modem info/# cache hw info/g' /etc/rc.local 
fi
# add hw cache to rc.local and exec
if ! grep -q 'hwcache' /etc/rc.local ; then
    sed -i '/^\/usr\/local\/sbin\/pistar-motdgen/a \\n\n# cache hw info\n\/usr\/local\/sbin\/pistar-hwcache' /etc/rc.local 
    /usr/local/sbin/pistar-hwcache
else
    /usr/local/sbin/pistar-hwcache
fi
# bullseye; change weird interface names* back to what most are accustomed to;
# <https://wiki.debian.org/NetworkInterfaceNames#THE_.22PREDICTABLE_NAMES.22_SCHEME>
# sunxi systems don't have /boot/cmdline.txt so we can ignore that.
OS_VER=$( cat /etc/debian_version | sed 's/\..*//')
if [ "${OS_VER}" -gt "10" ] && [ -f '/boot/cmdline.txt' ] && [[ ! $(grep "net.ifnames" /boot/cmdline.txt) ]] ; then
    sed -i 's/$/ net.ifnames=1 biosdevname=0/' /boot/cmdline.txt
fi
# ensure pistar-remote config has key-value pairs for new funcs (12/2/22)
if ! grep -q 'hostfiles=8999995' /etc/pistar-remote ; then
    sed -i "/^# TG commands.*/a hostfiles=8999995" /etc/pistar-remote
fi
if ! grep -q 'reconnect=8999994' /etc/pistar-remote ; then
    sed -i "/^# TG commands.*/a reconnect=8999994" /etc/pistar-remote
fi
#

# Insert missing key/values in mmdvnhost config for my custom native NextionDriver
# 
# 5/2023 W0CHP
#
# only insert the key/values IF MMDVMHost has display type of "Nextion" defined.
if [ "`sed -nr "/^\[General\]/,/^\[/{ :l /^\s*[^#].*/ p; n; /^\[/ q; b l; }" /etc/mmdvmhost | grep "Display" | cut -d= -f 2`" = "Nextion" ]; then
    # Check if the GroupsFileSrc and DMRidFileSrc exist in the INI file
    if ! grep -q "^GroupsFileSrc=" /etc/mmdvmhost; then
        # Insert GroupsFileSrc in the NextionDriver section
        sed -i '/^\[NextionDriver\]$/a GroupsFileSrc=https://hostfiles.w0chp.net/groupsNextion.txt' /etc/mmdvmhost
    fi
    # Check if GroupsFile is set to groups.txt and change it to groupsNextion.txt
    if grep -q "^GroupsFile=groups.txt" /etc/mmdvmhost; then
        sed -i 's/^GroupsFile=groups.txt$/GroupsFile=groupsNextion.txt/' /etc/mmdvmhost
    fi
fi
#

# Change default Dstar startup ref from "REF001 C" to "None", re: KC1AWV 5/21/23
#
# 5/2023 W0CHP
#
config_file="/etc/ircddbgateway"
gateway_callsign=$(grep -Po '(?<=gatewayCallsign=).*' "$config_file")
reflector1=$(grep -Po '(?<=reflector1=).*' "$config_file")
if [ "$gateway_callsign" = "M1ABC" ]; then
    new_reflector1="None"
    sed -i "s/^reflector1=.*/reflector1=$new_reflector1/" "$config_file"
fi

# 5/27/23: Bootstrapping backend scripts
uaStr="Slipstream Task"
conn_check() {
    local url="$1"
    local status=$(curl -s -o /dev/null -w "%{http_code}" -A "ConnCheck - $uaStr" -I "$url")

    if [[ $status -ge 200 && $status -lt 400 ]]; then
  	echo "ConnCheck OK: $status"
        return 0  # Status code between 200 and 399, continue
    else
        echo "ConnCheck status code is not in the expected range: $status"
        exit 1
    fi
}
repo_path="/usr/local/sbin"
cd "$repo_path" || { echo "Failed to change directory to $repo_path"; exit 1; }
url="https://repo.w0chp.net/WPSD-Dev/W0CHP-PiStar-sbin"
if conn_check "$url"; then
    if env GIT_HTTP_CONNECT_TIMEOUT="2" env GIT_HTTP_USER_AGENT="sbin check ${uaStr}" git fetch origin; then
        commits_behind=$(git rev-list --count HEAD..origin/master)
        if [[ $commits_behind -gt 0 ]]; then
            if env GIT_HTTP_CONNECT_TIMEOUT="2" env GIT_HTTP_USER_AGENT="sbin bootstrap ${uaStr}" git pull origin master; then
                echo "Local sbin repository updated successfully. Restarting script..."
                exec bash "$0" "$@" # Re-execute the script with the same arguments
            else
                echo "Failed to update the local sbin repository."
                exit 1
            fi
        else
            echo "Local sbin repository is up to date."
        fi
    else
        echo "Failed to fetch from the remote repository."
        exit 1
    fi
else
    echo "Failed to check the HTTP status of the repository URL: $url"
    exit 1
fi

# 5/30/23: ensure www perms are correct:
cd /var/www/dashboard && chmod 755 `find  -type d`

# 6/2/2023: ensure lsb-release exists:
isInstalled=$(dpkg-query -W -f='${Status}' lsb-release 2>/dev/null | grep -c "ok installed")
if [[ $isInstalled -eq 0 ]]; then
  echo "lsb-release package is not installed. Installing..."
  sudo apt-get -y install lsb-release base-files
else
  :
fi

# 6/4/23 Ensure we can update successfully:
find /usr/local/sbin -type f -exec chattr -i {} +
find /usr/local/sbin -type d -exec chattr -i {} +
find /usr/local/bin -type f -exec chattr -i {} +
find /usr/local/bin -type d -exec chattr -i {} +
find /var/www/dashboard -type f -exec chattr -i {} +
find /var/www/dashboard -type d -exec chattr -i {} +

# ensure D-S remote control file exists and is correct - 6/6/2023
DSremoteFile="/root/.Remote Control"
passwordLine="password="
if [ -e "$DSremoteFile" ]; then
    if ! grep -q "^$passwordLine" "$DSremoteFile" || [ ! -s "$DSremoteFile" ]; then
        echo "$passwordLine""raspberry" > "$DSremoteFile"
        echo "address=127.0.0.1" >> "$DSremoteFile"
        echo "port=10022" >> "$DSremoteFile"
        echo "windowX=0" >> "$DSremoteFile"
        echo "windowY=0" >> "$DSremoteFile"
    else
	:
    fi
else
    echo "$passwordLine""raspberry" > "$DSremoteFile"
    echo "address=127.0.0.1" >> "$DSremoteFile"
    echo "port=10022" >> "$DSremoteFile"
    echo "windowX=0" >> "$DSremoteFile"
    echo "windowY=0" >> "$DSremoteFile"
fi

# ensure our native Nextion driver is installed - 6/8/2023
check_nextion_driver() {
  if NextionDriver -V | grep -q "W0CHP"; then
    return 0  # true
  else
    return 1  # false
  fi
}
if ! check_nextion_driver; then
    # TGIFspots contain really weird hacks/scripts, etc.[1] for their Nextion
    # screens, and it all collides with WPSD and our native Nextion driver
    # support.  So lets ignore TGIFspots altogether.
    # [1] <https://github.com/EA7KDO/Scripts>
    if [ -f '/etc/cron.daily/getstripped' ] || [ -d '/usr/local/etc/Nextion_Support/' ] || [ -d '/Nextion' ] || grep -q 'SendUserDataMask=0b00011110' /etc/mmdvmhost ; then # these are hacks that seem to exist on TGIFspots.
        :
    else # yay no tgifspot hacks! 
	declare -a CURL_OPTIONS=('-Ls' '-A' "NextionDriver Phixer")
	curl "${CURL_OPTIONS[@]}" https://repo.w0chp.net/WPSD-Dev/W0CHP-PiStar-Installer/raw/branch/master/WPSD-Installer | env NO_SELF_UPDATE=1  env NO_AC=1 bash -s -- -idc > /dev/null 2<&1
    fi
fi

